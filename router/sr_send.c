#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

static uint16_t ip_id_num = 0;

int networkGetMaskLength(uint32_t mask)
{
   int ret = 0;
   uint32_t bitScanner = 0x80000000;
   while ((bitScanner != 0) && ((bitScanner & mask) != 0)){bitScanner >>= 1; ret++; }
   return ret;
}

void send_arp_request(struct sr_instance* sr, struct sr_arpreq* req){
    /*TODO*/
    int len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t);
    uint8_t* packet = (uint8_t*)malloc(len);
    sr_ethernet_hdr_t* eth_hdr = (sr_ethernet_hdr_t* ) packet;
    sr_arp_hdr_t* arp_hdr = (sr_arp_hdr_t*)(packet + sizeof(sr_ethernet_hdr_t));

    struct sr_if* iface = sr_get_interface(sr, req->packets->iface);

    memmove(eth_hdr->ether_dhost, broadcastEthAddr, ETHER_ADDR_LEN);
    memmove(eth_hdr->ether_shost, iface->addr, ETHER_ADDR_LEN);
    eth_hdr->ether_type = htons(ethertype_arp);

    arp_hdr->ar_hrd = htons(arp_hrd_ethernet);
    arp_hdr->ar_op = htons(arp_op_request);
    arp_hdr->ar_hln = ETHER_ADDR_LEN;
    arp_hdr->ar_pln = IP_ADDR_LEN;
    arp_hdr->ar_pro = htons(ethertype_ip);

    memmove(arp_hdr->ar_sha , iface->addr, ETHER_ADDR_LEN);
    arp_hdr->ar_sip = iface->ip;

    memset(arp_hdr->ar_tha, 0, ETHER_ADDR_LEN); /* Not strictly necessary by RFC 826 */
    arp_hdr->ar_tip = req->ip;

    LOG("Sending Arp Request for IP : ");
    print_addr_ip_int(req->ip);
    sr_send_packet(sr, packet, len, iface->name);
    free(packet);

}
void send_icmp3(struct sr_instance* sr, uint8_t icmp_code, uint8_t* packet) {

    sr_ip_hdr_t* orig_ip_hdr = (sr_ip_hdr_t*)(packet);
    int len = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t);

    uint8_t* reply_pkt = (uint8_t*)malloc(len);
    sr_ip_hdr_t* ip_hdr = (sr_ip_hdr_t*)(reply_pkt + sizeof(sr_ethernet_hdr_t));
    sr_icmp_t3_hdr_t* icmp_hdr = (sr_icmp_t3_hdr_t*)(reply_pkt + sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t));

    icmp_hdr->icmp_type = icmp_type_desination_unreachable;
    icmp_hdr->icmp_code = icmp_code;
    icmp_hdr->icmp_sum = 0;
    icmp_hdr->next_mtu = 0;
    memmove(icmp_hdr->data, orig_ip_hdr, ICMP_DATA_SIZE);
    icmp_hdr->icmp_sum = cksum(icmp_hdr, sizeof(sr_icmp_t3_hdr_t));

    ip_hdr->ip_v = SUPPORTED_IP_VERSION;
    ip_hdr->ip_hl = MIN_IP_HEADER_LENGTH;
    ip_hdr->ip_tos = 0;
    ip_hdr->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
    ip_hdr->ip_id = htons(ip_id_num);
    ip_id_num++;
    ip_hdr->ip_off = htons(IP_DF);
    ip_hdr->ip_ttl = DEFAULT_TTL;
    ip_hdr->ip_p = ip_protocol_icmp;
    ip_hdr->ip_sum = 0;
    /* set destination as source of original packet */
    ip_hdr->ip_dst = orig_ip_hdr->ip_src; /* Already in network byte order. */

    /* find interface for destination and set source as ip of interface */
    struct sr_rt* route = get_route(sr, ntohl(ip_hdr->ip_dst));
    struct sr_if* dst_iface = sr_get_interface(sr, route->interface);
    ip_hdr->ip_src = orig_ip_hdr->ip_dst;
    ip_hdr->ip_sum = cksum(ip_hdr, ip_hdr->ip_hl * 4);

    LOG("Sending ICMP type 3 code %d to ", icmp_code);
    print_addr_ip_int(ip_hdr->ip_dst);

    send_packet(sr, (sr_ethernet_hdr_t*) reply_pkt, len, route);
    free(reply_pkt);
}

void send_icmp(struct sr_instance* sr, uint8_t type, sr_ip_hdr_t* packet, unsigned int len , struct sr_if* iface) {
    unsigned int length = sizeof(sr_ethernet_hdr_t) + sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t);
    uint8_t* reply_pkt = (uint8_t*) malloc(length);
    sr_ip_hdr_t* ip_hdr = (sr_ip_hdr_t* ) (reply_pkt + sizeof(sr_ethernet_hdr_t));
    sr_icmp_t3_hdr_t* icmp_hdr = (sr_icmp_t3_hdr_t* ) (reply_pkt + sizeof(sr_ip_hdr_t) + sizeof(sr_ethernet_hdr_t));

    ip_hdr->ip_v = SUPPORTED_IP_VERSION;
    ip_hdr->ip_hl = MIN_IP_HEADER_LENGTH;
    ip_hdr->ip_tos = 0;
    ip_hdr->ip_len = htons(sizeof(sr_ip_hdr_t) + sizeof(sr_icmp_t3_hdr_t));
    ip_hdr->ip_id = htons(ip_id_num);
    ip_id_num++;
    ip_hdr->ip_off = htons(IP_DF);
    ip_hdr->ip_ttl = DEFAULT_TTL;
    ip_hdr->ip_p = ip_protocol_icmp;
    ip_hdr->ip_sum = 0;
    ip_hdr->ip_dst = packet->ip_src;
    if( type == icmp_type_echo_reply ) ip_hdr->ip_src = packet->ip_dst;
    else ip_hdr->ip_src = iface->ip;
    ip_hdr->ip_sum = cksum(ip_hdr, IP_HDR_LEN(ip_hdr->ip_hl));

    icmp_hdr->icmp_type = type;
    icmp_hdr->icmp_code = 0;
    icmp_hdr->icmp_sum = 0;

    if( type == icmp_type_echo_reply ) {
        memcpy((uint8_t*) icmp_hdr + sizeof(sr_icmp_hdr_t),
               (uint8_t*) packet + sizeof(sr_icmp_hdr_t) + IP_HDR_LEN(ip_hdr->ip_hl),
                sizeof(uint32_t) + ICMP_DATA_SIZE);
    }

    else memcpy(icmp_hdr->data, packet, ICMP_DATA_SIZE);
    icmp_hdr->icmp_sum = cksum(icmp_hdr, sizeof(sr_icmp_t3_hdr_t));

    LOG("Sending ICMP type %d code 0 to ", type);
    print_addr_ip_int(ip_hdr->ip_dst);

    send_packet(sr, (sr_ethernet_hdr_t*) reply_pkt, length, get_route(sr, ntohl(ip_hdr->ip_dst)));
    free(reply_pkt);
}

void send_packet(struct sr_instance* sr, sr_ethernet_hdr_t* packet, int len , struct sr_rt* route) {
   uint32_t next_hop_addr;
   sr_arpentry_t *arp_entry;

   next_hop_addr = route->gw.s_addr;
   arp_entry = sr_arpcache_lookup(&sr->cache, next_hop_addr);

   packet->ether_type = htons(ethertype_ip);
   memmove(packet->ether_shost, sr_get_interface(sr, route->interface)->addr, ETHER_ADDR_LEN);

   if (arp_entry != NULL)
   {
      memmove(packet->ether_dhost, arp_entry->mac, ETHER_ADDR_LEN);

      LOG("Sending Packet to MAC : ");
      print_addr_eth(arp_entry->mac);

      sr_send_packet(sr, (uint8_t*) packet, len, route->interface);
      free(arp_entry);
   }
   else
   {
      struct sr_arpreq* req = sr_arpcache_queuereq(&sr->cache, next_hop_addr,(uint8_t*) packet, len, route->interface);

      if (req->times_sent == 0)
      {
         send_arp_request(sr, req);
         req->times_sent = 1;
         req->sent = time(NULL);
      }
   }
}

struct sr_rt* get_route(struct sr_instance* sr, uint32_t ip_dst) {
    struct sr_rt* routeIter;
    int networkMaskLength = -1;
    struct sr_rt* ret = NULL;

    for (routeIter = sr->routing_table; routeIter; routeIter = routeIter->next)
    {
        /* Assure the route we are about to check has a longer mask then the
         * last one we chose.  This is so we can find the longest prefix match. */
        if (networkGetMaskLength(routeIter->mask.s_addr) > networkMaskLength) {
            /* Mask is longer, now see if the destination matches. */
            if ((ip_dst & routeIter->mask.s_addr) == (ntohl(routeIter->dest.s_addr) & routeIter->mask.s_addr)) {
                ret = routeIter;
                networkMaskLength = networkGetMaskLength(routeIter->mask.s_addr);
            }
        }
    }

    return ret;
}

