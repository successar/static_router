/**********************************************************************
 * file:  sr_router.c
 * date:  Mon Feb 18 12:50:42 PST 2002
 * Contact: casado@stanford.edu
 *
 * Description:
 *
 * This file contains all the functions that interact directly
 * with the routing table, as well as the main entry method
 * for routing.
 *
 **********************************************************************/

#include <stdio.h>
#include <assert.h>


#include "sr_if.h"
#include "sr_rt.h"
#include "sr_router.h"
#include "sr_protocol.h"
#include "sr_arpcache.h"
#include "sr_utils.h"

int is_final(struct sr_instance* sr, sr_ip_hdr_t * header) {
	struct sr_if * cur_iface = sr->if_list;
	while (cur_iface != NULL) {
		if (header->ip_dst == cur_iface->ip) {
			return 1;
		}
		cur_iface = cur_iface->next;
	}
	return 0;
}

/*---------------------------------------------------------------------
 * Method: sr_init(void)
 * Scope:  Global
 *
 * Initialize the routing subsystem
 *
 *---------------------------------------------------------------------*/

void sr_init(struct sr_instance* sr) {
    /* REQUIRES */
    assert(sr);

    /* Initialize cache and cache cleanup thread */
    freopen("log_msg", "w+", stderr);
    sr_arpcache_init(&(sr->cache));

    pthread_attr_init(&(sr->attr));
    pthread_attr_setdetachstate(&(sr->attr), PTHREAD_CREATE_JOINABLE);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setscope(&(sr->attr), PTHREAD_SCOPE_SYSTEM);
    pthread_t thread;

    pthread_create(&thread, &(sr->attr), sr_arpcache_timeout, sr);

    /* Add initialization code here! */

} /* -- sr_init -- */

/*---------------------------------------------------------------------
 * Method: sr_handlepacket(uint8_t* p,char* interface)
 * Scope:  Global
 *
 * This method is called each time the router receives a packet on the
 * interface.  The packet buffer, the packet length and the receiving
 * interface are passed in as parameters. The packet is complete with
 * ethernet headers.
 *
 * Note: Both the packet buffer and the character's memory are handled
 * by sr_vns_comm.c that means do NOT delete either.  Make a copy of the
 * packet instead if you intend to keep it around beyond the scope of
 * the method call.
 *
 *---------------------------------------------------------------------*/

void sr_handlepacket(struct sr_instance* sr,
                     uint8_t* packet/* lent */,
                     unsigned int len,
                     char* interface/* lent */) {
    /* REQUIRES */
    assert(sr);
    assert(packet);
    assert(interface);

    printf("*** -> Received packet of length %d \n",len);
    sr_ethernet_hdr_t* eth_hdr = (sr_ethernet_hdr_t*) packet;

    /* fill in code here */
    if( len < sizeof(sr_ethernet_hdr_t)) {
        LOG("packet not long enough to hold eth header\n");
        return;
    }

    struct sr_if* iface = sr_get_interface(sr, interface);
    if ((iface == NULL) || ((memcmp(eth_hdr->ether_dhost, iface->addr, ETHER_ADDR_LEN) != 0)
                            && (memcmp(eth_hdr->ether_dhost, broadcastEthAddr, ETHER_ADDR_LEN) != 0))) {
        LOG("Invalid ethernet Address : \n");
        print_addr_eth(eth_hdr->ether_dhost);
        return;
    }

    uint16_t eth_type = ethertype(packet);
    uint8_t* str_packet = packet + sizeof(sr_ethernet_hdr_t);
    unsigned int str_len = len - sizeof(sr_ethernet_hdr_t);

    switch(eth_type) {
        case ethertype_arp :
            sr_handle_arp(sr, (sr_arp_hdr_t*) str_packet, str_len, iface);
            break;
        case ethertype_ip :
            sr_handle_ip(sr, (sr_ip_hdr_t*) str_packet, str_len, iface);
            break;
        default :
            LOG("Not a valid ethernet Type : %d\n", eth_type);
    }

    fprintf(stderr, "\n");

    fflush(stderr);

}/* end sr_ForwardPacket */


void sr_handle_arp(struct sr_instance* sr, sr_arp_hdr_t* packet, unsigned int len, struct sr_if* iface) {
    if (len < sizeof(sr_arp_hdr_t)) {
        LOG("packet not big enough to hold arp header\n");
        return;
    }

    if ((ntohs(packet->ar_pro) != ethertype_ip) || (ntohs(packet->ar_hrd) != arp_hrd_ethernet)
            || (packet->ar_pln != IP_ADDR_LEN) || (packet->ar_hln != ETHER_ADDR_LEN))  {
        LOG("ARP packet received with invalid parameters\n");
        return;
    }

    unsigned short arp_op = ntohs(packet->ar_op);
    switch(arp_op)    {
        case arp_op_request :
            sr_handle_arp_request(sr, packet, len, iface);
            break;
        case arp_op_reply :
            LOG("Received Arp Reply\n");
            sr_handle_arp_reply(sr, packet, len, iface);
            break;
        default:
            LOG("Invalid Arp Op : %d\n", arp_op);
    }

}

void sr_handle_ip(struct sr_instance* sr, sr_ip_hdr_t* packet, unsigned int len, struct sr_if* iface) {
    if (len < sizeof(sr_ip_hdr_t))    {
        LOG("packet not big enough to hold ip header\n");
        return;
    }
    if( packet->ip_hl < MIN_IP_HEADER_LENGTH ) {
        LOG("IP header length ip_hl insufficient\n");
        return;
    }

    uint16_t hdr_cksum = packet->ip_sum;
    uint16_t calc_cksum = 0;
    packet->ip_sum = 0;
    calc_cksum = cksum(packet, IP_HDR_LEN(packet->ip_hl));

    if (hdr_cksum != calc_cksum) {
        LOG("IP checksum failed. Dropping received packet.\n");
        return;
    } else packet->ip_sum = hdr_cksum;

    if (packet->ip_v != SUPPORTED_IP_VERSION) {
        LOG("Received non-IPv4 packet. Dropping.\n");
        return;
    }

    LOG("Received IP packet from IP : ");
    print_addr_ip_int(packet->ip_src);

    if( is_final(sr, packet)) {
        uint8_t ip_proto = ip_protocol((uint8_t*) packet);
		if (ip_proto == ip_protocol_icmp) sr_handle_icmp(sr, packet , len , iface);
		else send_icmp3(sr, icmp_code_destination_port_unreachable,(uint8_t*) packet);
    }
    else {
        uint8_t ttl = packet->ip_ttl - 1;
        if( ttl == 0 ) { send_icmp(sr, icmp_type_time_exceeded, packet, len, iface); return; }
        packet->ip_ttl = ttl;
        packet->ip_sum = 0;
        packet->ip_sum = cksum(packet, IP_HDR_LEN(packet->ip_hl));
        struct sr_rt* route = get_route(sr, ntohl(packet->ip_dst));

        if( route == NULL ) send_icmp3(sr, icmp_code_network_unreachable, (uint8_t*) packet);

        LOG("Forwarding packet to IP : ");
        print_addr_ip_int(packet->ip_dst);

        uint8_t* forward_pkt = (uint8_t*) malloc(len + sizeof(sr_ethernet_hdr_t));
        memcpy(forward_pkt + sizeof(sr_ethernet_hdr_t), packet, len);

        send_packet(sr, (sr_ethernet_hdr_t*) forward_pkt, len + sizeof(sr_ethernet_hdr_t), route);
        free(forward_pkt);
    }

}

void sr_handle_icmp(struct sr_instance* sr, sr_ip_hdr_t* packet, unsigned int len, struct sr_if* iface) {
    sr_icmp_hdr_t* icmp_hdr = (sr_icmp_hdr_t*) ((uint8_t*)packet + IP_HDR_LEN(packet->ip_hl));
    int icmp_len = len - IP_HDR_LEN(packet->ip_hl);

    uint16_t hdr_cksum = icmp_hdr->icmp_sum;
    uint16_t calc_cksum = 0;
    icmp_hdr->icmp_sum = 0;
    calc_cksum = cksum(icmp_hdr, icmp_len);

    if (hdr_cksum != calc_cksum) {
        LOG("ICMP checksum failed. Dropping received packet.\n");
        return;
    } else icmp_hdr->icmp_sum = hdr_cksum;

    if(icmp_hdr->icmp_type == icmp_type_echo_request) {
        LOG("Received icmp ECHO REQUEST\n");
        send_icmp(sr, icmp_type_echo_reply, packet, len, iface);
    }
    else {
        LOG("ICMP type not supported : %d\n", icmp_hdr->icmp_type);
    }

}

void sr_handle_arp_request(struct sr_instance* sr, sr_arp_hdr_t* packet, unsigned int len, struct sr_if* iface) {
    if (packet->ar_tip == iface->ip) {
        uint8_t* reply_pkt = (uint8_t*) malloc(sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t));
        sr_ethernet_hdr_t* eth_hdr = (sr_ethernet_hdr_t*)reply_pkt;
        sr_arp_hdr_t* arp_hdr = (sr_arp_hdr_t*)(reply_pkt + sizeof(sr_ethernet_hdr_t));

        LOG("Received Arp Request from IP : ");
        print_addr_ip_int(packet->ar_sip);

        /* Ethernet Header */
        memcpy(eth_hdr->ether_dhost, packet->ar_sha, ETHER_ADDR_LEN);
        memcpy(eth_hdr->ether_shost, iface->addr, ETHER_ADDR_LEN);
        eth_hdr->ether_type = htons(ethertype_arp);

        /* ARP Header */
        arp_hdr->ar_hrd = htons(arp_hrd_ethernet);
        arp_hdr->ar_pro = htons(ethertype_ip);
        arp_hdr->ar_hln = ETHER_ADDR_LEN;
        arp_hdr->ar_pln = IP_ADDR_LEN;
        arp_hdr->ar_op = htons(arp_op_reply);
        memcpy(arp_hdr->ar_sha, iface->addr, ETHER_ADDR_LEN);
        arp_hdr->ar_sip = iface->ip;
        memcpy(arp_hdr->ar_tha, packet->ar_sha, ETHER_ADDR_LEN);
        arp_hdr->ar_tip = packet->ar_sip;

        LOG("Sending Arp Reply from interface IP : ");
        print_addr_ip_int(iface->ip);

        sr_send_packet(sr, reply_pkt, sizeof(sr_ethernet_hdr_t) + sizeof(sr_arp_hdr_t), iface->name);
        free(reply_pkt);
    }
}

void sr_handle_arp_reply(struct sr_instance* sr, sr_arp_hdr_t* packet, unsigned int len, struct sr_if* iface) {
    if (packet->ar_tip == iface->ip) {
        struct sr_arpreq* req = sr_arpcache_insert(&sr->cache, packet->ar_sha, packet->ar_sip);
        if (req != NULL) {

            LOG("Sending all queued packets to MAC : ");
            print_addr_eth(packet->ar_sha);

            while (req->packets != NULL) {
                struct sr_packet* curr = req->packets;

                /* Copy address from reply into packets detination eth addr */
                memcpy(((sr_ethernet_hdr_t*) curr->buf)->ether_dhost, packet->ar_sha, ETHER_ADDR_LEN);
                sr_send_packet(sr, curr->buf, curr->len, curr->iface);

                req->packets = req->packets->next;
                free(curr->buf);
                free(curr->iface);
                free(curr);
            }
            sr_arpreq_destroy(&sr->cache, req);
        } else {
            LOG("Found no request.\n");
        }
    }
}
